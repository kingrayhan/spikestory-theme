<?php

$options[] = array(
    'name' => 'social',
    'title' => 'Social Media',
    'icon' => 'fa fa-twitter',

    // begin: fields
    'fields' => array(

        array(
            'id'              => 'social_links',
            'type'            => 'group',
            'title'           => 'Social Links',
            'button_title'    => 'Add New',
            'accordion_title' => 'Add New socail link',
            'fields'          => array(
                array(
                    'id'    => 'social_name',
                    'type'  => 'text',
                    'title' => 'Social Network Name',
                ),
                array(
                    'id'    => 'social_link',
                    'type'  => 'text',
                    'title' => 'Social Link',
                ),
                array(
                    'id'    => 'social_icon',
                    'type'  => 'icon',
                    'title' => 'Social Icon',
                )
            ),
        )



    ), // end: fields
);

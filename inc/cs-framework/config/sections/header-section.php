<?php
/**
 * File: header-section.php
 * Written By: @KingRayhan <https://github.com/kingRayhan>
 * 1/10/2018 | 4:07 PM
 */

$options[] = array(
    'name' => 'header',
    'title' => 'Header',
    'icon' => 'fa fa-cog',
    'fields' => array(
        array(
            'id' => 'site-logo',
            'type' => 'upload',
            'title' => 'Upload Field',
            'default' => get_template_directory_uri() . '/assets/images/site-logo.png',
            'settings' => array(
                'upload_type' => 'image',
                'button_title' => 'Upload Logo',
                'frame_title' => 'Select an image',
                'insert_title' => 'Use image as logo',
            ),
        ),
    )
);
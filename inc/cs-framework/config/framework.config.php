<?php if (!defined('ABSPATH')) {
    die;
} // Cannot access pages directly.
// ===============================================================================================
// -----------------------------------------------------------------------------------------------
// FRAMEWORK SETTINGS
// -----------------------------------------------------------------------------------------------
// ===============================================================================================
$settings = array(
    'menu_title' => 'Theme Options',
    'menu_type' => 'menu', // menu, submenu, options, theme, etc.
    'menu_slug' => 'theme-options',
    'ajax_save' => false,
    'show_reset_all' => false,
    'framework_title' => 'Spike Story',
);

// ===============================================================================================
// -----------------------------------------------------------------------------------------------
// FRAMEWORK OPTIONS
// -----------------------------------------------------------------------------------------------
// ===============================================================================================
$options = array();


require_once get_template_directory() . '/inc/cs-framework/config/sections/header-section.php';
require_once get_template_directory() . '/inc/cs-framework/config/sections/social.php';

CSFramework::instance($settings, $options);

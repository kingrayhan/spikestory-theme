<footer id="footer">
    <section class="container" id="footer-widgets">
        <div class="container-inner">
            <div class="pad group">
                <div class="footer-widget-1 grid one-third ">
                    <div id="custom_html-2" class="widget_text widget widget_custom_html">

                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="container-fluid" id="footer-bottom">
        <div class="container-inner"><a id="back-to-top" href="#"><i class="fa fa-angle-up"></i></a>
            <div class="pad group">
                <div class="grid one-half"><img id="footer-logo" src="images/insights-logo.png" alt="">
                    <div id="copyright">
                        <p>© Copyright 2017 Sendible. All Rights Reserved.</p>
                    </div>
                </div>
                <div class="grid one-half last">
                    <ul class="social-links">
                        <?php foreach (cs_get_option('social_links') as $social): ?>
                            <li>
                                <a rel="nofollow" class="social-tooltip" title="<?= $social['social_name'] ?>"
                                   href="<?= $social['social_link'] ?>" target="_bkank">
                                    <i class="<?= $social['social_icon'] ?>"></i>
                                </a>
                            </li>
                        <?php endforeach; ?>
                    </ul>
                </div>
            </div>
        </div>
    </section>
</footer>
</div>

<?php wp_footer(); ?>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/js/bootstrap.min.js" integrity="sha384-a5N7Y/aK3qNeh15eJKGWxsqtnX/wWdSZSKp+81YjTmS15nvnvxKHuzaWwXHDli+4" crossorigin="anonymous"></script>

</body>
</html>
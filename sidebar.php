                        <div class="sidebar s1"> <a class="sidebar-toggle" title="Expand Sidebar"><i class="fa icon-sidebar-toggle"></i></a>
                            <div class="sidebar-content">
                                <div class="sidebar-top group">
                                    <p>Follow:</p>
                                    <ul class="social-links" style="text-align: left;">
                                        <?php foreach (cs_get_option('social_links') as $social): ?>
                                            <li>
                                                <a rel="nofollow" class="social-tooltip" title="<?= $social['social_name'] ?>"
                                                   href="<?= $social['social_link'] ?>" target="_bkank">
                                                    <i class="<?= $social['social_icon'] ?>"></i>
                                                </a>
                                            </li>
                                        <?php endforeach; ?>
                                    </ul>
                                </div>
                                <div id="text-25" class="widget widget_text">
                                    <div class="textwidget">
                                        <div class="webinar-widget">
                                            <h3>Blog: 16 Marketing Interview Questions to Hire the Best Cultural Fit</h3> <span> <i class="fa fa-hourglass-o"></i> 10-minute read </span>
                                            <p> Looking to hire new talent for your marketing team?
                                                <br>
                                                <br> Making the wrong hire can be a major setback. We have collated some of the best job interview questions you can ask to better understand the personality and skills of your candidates.</p> <a class="btn primary" href="best-social-media-manager-qualities.html" target="_blank">Read the blog</a></div>
                                    </div>
                                </div>
                                <div id="wpdevart_countdown-2" class="widget wpdevart_countdown">
                                    <div class="content_countdown" id="main_countedown_widget_2">
                                        <div class="countdown"> <span class="element_conteiner"><span class="days time_left">-41</span><span class="time_description">DD</span></span> <span class="element_conteiner"><span class="hourse time_left">-3</span><span class="time_description">HH</span></span> <span class="element_conteiner"><span class="minutes time_left">-47</span><span class="time_description">MM</span></span> <span class="element_conteiner"><span class="secondes time_left">-14</span><span class="time_description">SS</span></span>
                                        </div>
                                    </div>
                                    <script>
                                        jQuery(document).ready(function() {
                                            jQuery('#main_countedown_widget_2 .countdown').html('')
                                        });
                                    </script>
                                    <style>
                                        #main_countedown_widget_2 .countdown {
                                            text-align: left
                                        }
                                        
                                        #main_countedown_widget_2 .countdown {
                                            margin-top: 0px;
                                            margin-bottom: 10px
                                        }
                                        
                                        #main_countedown_widget_2 .time_left {
                                            border-radius: 8px;
                                            background-color: #3DA8CC;
                                            font-size: 20px;
                                            font-family: monospace;
                                            color: #000
                                        }
                                        
                                        #main_countedown_widget_2 .time_description {
                                            font-size: 20px;
                                            font-family: monospace;
                                            color: #000
                                        }
                                        
                                        #main_countedown_widget_2 .element_conteiner {
                                            min-width: 73px
                                        }
                                    </style>
                                </div>
                                <div id="salesforce-3" class="widget salesforce">
                                    <div class="salesforce_w2l_lead">
                                        <form id="sf_form_salesforce_w2l_lead_22_sidebar" class="w2llead sidebar placeholders" method="post" action="#sf_form_salesforce_w2l_lead_22_sidebar">
                                            <div class="sf_field sf_field_form-title sf_type_html">
                                                <br>
                                                <h3>Subscribe to our newsletter</h3>
                                                <div class="clearfix"></div>
                                            </div>
                                            <div class="sf_field sf_field_email sf_type_email">
                                                <input type="email" placeholder="Your email *" value="" id="sf_email" class="w2linput text" name="email">
                                                <div class="clearfix"></div>
                                            </div>
                                            <input type="hidden" id="sf_first_name" class="w2linput hidden" name="first_name" value="Colleague">
                                            <input type="hidden" id="sf_last_name" class="w2linput hidden" name="last_name" value="[not provided]">
                                            <input type="hidden" id="sf_Lead_Type__c" class="w2linput hidden" name="Lead_Type__c" value="Newsletter">
                                            <input type="text" name="message" class="w2linput" value="" style="display: none;">
                                            <input type="hidden" name="form_id" class="w2linput" value="22">
                                            <div class="w2lsubmit">
                                                <input type="submit" name="w2lsubmit" class="w2linput submit" value="Submit">
                                            </div>
                                        </form>
                                        <p class="sf_required_fields_msg" id="requiredfieldsmsg"><sup><span class="required">*</span></sup> These fields are required.</p>
                                    </div>
                                </div>
                                <div id="text-26" class="widget widget_text">
                                    <div class="textwidget">
                                        <style>
                                            #requiredfieldsmsg {
                                                display: none !important
                                            }
                                            
                                            #salesforce-2 {
                                                background: gainsboro
                                            }
                                            
                                            .post-message {
                                                color: #444 !important
                                            }
                                        </style>
                                        <script type="text/javascript">
                                            /*<![CDATA[*/
                                            var currentdate = 0;
                                            var image_number = 0;

                                            function ImageArray(n) {
                                                this.length = n;
                                                for (var i = 1; i <= n; i++) {
                                                    this[i] = {};
                                                }
                                            }
                                            image = new ImageArray(3);
                                            image[0] = {
                                                url: "http://assets.sendible.com.s3.amazonaws.com/blog/setup/banner-insights-1.png",
                                                a: "https://sendible.com/request-a-demo?afmc=1h"
                                            };
                                            image[1] = {
                                                url: "http://assets.sendible.com.s3.amazonaws.com/blog/setup/banner-insights-2.png",
                                                a: "https://sendible.com/features?afmc=1h"
                                            };
                                            image[2] = {
                                                url: "http://assets.sendible.com.s3.amazonaws.com/blog/setup/banner-insights-3.png",
                                                a: "https://sendible.com/features/social-media-analytics?afmc=1h"
                                            };
                                            var rand = 60 / image.length;

                                            function randomimage() {
                                                currentdate = new Date();
                                                image_number = currentdate.getSeconds();
                                                image_number = Math.floor(image_number / rand);
                                                return (image[image_number]);
                                            }
                                            var img = randomimage();
                                            document.write("<a target='_blank' href='" + img.a + "'><img src='" + img.url + "'></a>"); /*]]>*/
                                        </script>
                                    </div>
                                </div>
                            </div>
                        </div>
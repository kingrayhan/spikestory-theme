<!DOCTYPE html>
<html class="no-js" lang="en-US" xmlns:og="http://ogp.me/ns#" xmlns:fb="http://ogp.me/ns/fb#">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/css/bootstrap.min.css"
          integrity="sha384-Zug+QiDoJOrZ5t4lssLdxGhVrurbmBWopoEl+M6BdEfwnCJZtKxi1KgxUyJq13dy" crossorigin="anonymous">

    <?php wp_head(); ?>
    <style type="text/css">
        body {
            font-family: "Lato", Arial, sans-serif
        }

        .sidebar .widget {
            padding-left: 20px;
            padding-right: 20px;
            padding-top: 20px
        }

        ::selection {
            background-color: #2689de
        }

        ::-moz-selection {
            background-color: #2689de
        }

        a,
        .themeform label .required,
        #flexslider-featured .flex-direction-nav .flex-next:hover,
        #flexslider-featured .flex-direction-nav .flex-prev:hover,
        .post-hover:hover .post-title a,
        .post-title a:hover,
        .s1 .post-nav li a:hover i,
        .content .post-nav li a:hover i,
        .post-related a:hover,
        .s1 .widget_rss ul li a,
        #footer .widget_rss ul li a,
        .s1 .widget_calendar a,
        #footer .widget_calendar a,
        .s1 .alx-tab .tab-item-category a,
        .s1 .alx-posts .post-item-category a,
        .s1 .alx-tab li:hover .tab-item-title a,
        .s1 .alx-tab li:hover .tab-item-comment a,
        .s1 .alx-posts li:hover .post-item-title a,
        #footer .alx-tab .tab-item-category a,
        #footer .alx-posts .post-item-category a,
        #footer .alx-tab li:hover .tab-item-title a,
        #footer .alx-tab li:hover .tab-item-comment a,
        #footer .alx-posts li:hover .post-item-title a,
        .comment-tabs li.active a,
        .comment-awaiting-moderation,
        .child-menu a:hover,
        .child-menu .current_page_item > a,
        .wp-pagenavi a {
            color: #2689de
        }

        .themeform input[type="submit"],
        .themeform button[type="submit"],
        .s1 .sidebar-top,
        .s1 .sidebar-toggle,
        #flexslider-featured .flex-control-nav li a.flex-active,
        .post-tags a:hover,
        .s1 .widget_calendar caption,
        #footer .widget_calendar caption,
        .author-bio .bio-avatar:after,
        .commentlist li.bypostauthor > .comment-body:after,
        .commentlist li.comment-author-admin > .comment-body:after {
            background-color: #2689de
        }

        .post-format .format-container {
            border-color: #2689de
        }

        .s1 .alx-tabs-nav li.active a,
        #footer .alx-tabs-nav li.active a,
        .comment-tabs li.active a,
        .wp-pagenavi a:hover,
        .wp-pagenavi a:active,
        .wp-pagenavi span.current {
            border-bottom-color: #2689de !important
        }

        .s2 .post-nav li a:hover i,
        .s2 .widget_rss ul li a,
        .s2 .widget_calendar a,
        .s2 .alx-tab .tab-item-category a,
        .s2 .alx-posts .post-item-category a,
        .s2 .alx-tab li:hover .tab-item-title a,
        .s2 .alx-tab li:hover .tab-item-comment a,
        .s2 .alx-posts li:hover .post-item-title a {
            color: #232436
        }

        .s2 .sidebar-top,
        .s2 .sidebar-toggle,
        .post-comments,
        .jp-play-bar,
        .jp-volume-bar-value,
        .s2 .widget_calendar caption {
            background-color: #232436
        }

        .s2 .alx-tabs-nav li.active a {
            border-bottom-color: #232436
        }

        .post-comments span:before {
            border-right-color: #232436
        }

        .search-expand,
        #nav-topbar.nav-container {
            background-color: #232436
        }

        @media only screen and (min-width: 720px) {
            #nav-topbar .nav ul {
                background-color: #232436
            }
        }

        #header {
            background-color: #318fd8
        }

        @media only screen and (min-width: 720px) {
            #nav-header .nav ul {
                background-color: #318fd8
            }
        }

        #footer-bottom {
            background-color: #232436
        }

        .site-title a img {
            max-height: 70px
        }

        body {
            background-color: #fff
        }

        #nav-topbar .nav {
            width: inherit;
            float: none;
            text-align: center;
            display: block;
        }

        #nav-topbar.nav-container {
            position: absolute;
        }

        #nav-topbar .nav > li {
            float: none;
            border-right: none;
        }

        #nav-topbar {
            margin-top: 75px;
        }

        @media all and (max-width: 950px) {
            #nav-topbar {
                margin-top: 0px;
            }

            #nav-topbar.nav-container {
                background-color: transparent !important;
                box-shadow: none
            }
        }

        .nav-toggle i {
            font-size: 60px;
            padding: 10px 0;
        }

    </style>

</head>

<body class="home blog col-2cl full-width topbar-enabled unknown">
<div id="wrapper">
    <section>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="logo-wrap text-center">
                        <a href="#">
                            <img src="<?php echo cs_get_option('site-logo'); ?>">
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <nav class="nav-container group" id="nav-topbar">
            <div class="container-inner">
                <div class="nav-toggle"><i class="fa fa-bars"></i></div>
                <div class="nav-wrap container">
                    <?php
                    wp_nav_menu(array(
                        'theme_location' => 'mainmenu',
                        'menu_class' => 'nav container-inner ml-auto mr-auto group',
                        'menu_id' => 'menu-top-menu',
                        'container' => null
                    ));
                    ?>
                </div>
            </div>
        </nav>
    </section>

    <header id="header">
        <?php echo get_template_part('template-parts/jumbotron'); ?>
    </header>

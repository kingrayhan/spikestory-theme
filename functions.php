<?php
/**
 * spikestory functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package spikestory
 */

if ( ! function_exists( 'spikestory_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function spikestory_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on spikestory, use a find and replace
		 * to change 'spikestory' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'spikestory', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus( array(
			'mainmenu' => esc_html__( 'Primary', 'spikestory' ),
		) );

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		) );

		// Set up the WordPress core custom background feature.
		add_theme_support( 'custom-background', apply_filters( 'spikestory_custom_background_args', array(
			'default-color' => 'ffffff',
			'default-image' => '',
		) ) );

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support( 'custom-logo', array(
			'height'      => 250,
			'width'       => 250,
			'flex-width'  => true,
			'flex-height' => true,
		) );
	}
endif;
add_action( 'after_setup_theme', 'spikestory_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function spikestory_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'spikestory_content_width', 640 );
}
add_action( 'after_setup_theme', 'spikestory_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function spikestory_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'spikestory' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'spikestory' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'spikestory_widgets_init' );


/*


    <link rel="stylesheet" id="dashicons-css" href="<?php echo get_template_directory_uri(); ?>/assets/css/dashicons.min.css" type="text/css" media="all">
    <link rel="stylesheet" id="thickbox-css" href="<?php echo get_template_directory_uri(); ?>/assets/css/thickbox.css" type="text/css" media="all">
    <link rel="stylesheet" id="sfwp2lcss-css" href="<?php echo get_template_directory_uri(); ?>" type="text/css" media="all">

 */


/**
 * Enqueue scripts and styles.
 */
function spikestory_scripts() {
	wp_enqueue_style( 'spikestory-style', get_stylesheet_uri() );

	wp_enqueue_style( 'css-css', get_template_directory_uri() . '/assets/css/css.css' );
	wp_enqueue_style( 'style_1', get_template_directory_uri() . '/assets/css/style_1.css' );
	wp_enqueue_style( 'responsive', get_template_directory_uri() . '/assets/css/responsive.css' );
	wp_enqueue_style( 'fontawesome', 'https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css' );
	wp_enqueue_style( 'iconmoon', get_template_directory_uri() .'/assets/css/icomoon-font.css' );
	wp_enqueue_style( 'about-author', get_template_directory_uri() .'/assets/css/wp-about-author.css' );
	wp_enqueue_style( 'about-author', get_template_directory_uri() .'/assets/css/sfwp2l.css' );
	wp_enqueue_style( 'theme-style', get_template_directory_uri() .'/assets/css/style.css' );



    wp_enqueue_script('flexslider', get_template_directory_uri() . '/assets/js/jquery.flexslider.min.js', array('jquery'), false, false);
    wp_enqueue_script('theme-script', get_template_directory_uri() . '/assets/js/scripts.js', array('jquery'), false, true);




	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'spikestory_scripts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/inc/jetpack.php';
}


require_once get_template_directory() .'/inc/cs-framework/cs-framework.php';



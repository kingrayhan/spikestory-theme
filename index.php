<?php get_header();
$counter = 0;
$fet_flag = 0;
?>
    <div class="container-fluid" id="page">
        <div class="container-inner">
            <div class="main">
                <div class="main-inner group">
                    <section class="content">
                        <div class="pad group">
                            <?php if (have_posts()) : while (have_posts()) : the_post();
                                $fet_flag++;
                                if ($fet_flag !== 1) continue;
                                ?>
                                <?php echo get_template_part('template-parts/content-fet-post'); ?>
                            <?php endwhile; ?>
                                <!-- post navigation -->
                            <?php else: ?>
                                <!-- no posts found -->
                            <?php endif; ?>
                            <div class="post-list group">
                                <div class="post-row">
                                    <?php
                                    if( class_exists('AjaxLoadMore') )
                                        echo do_shortcode('[ajax_load_more]');

                                    ?>
                                </div>

                            </div><!-- .post-list group -->
                        </div>
                    </section>
                    <?php get_sidebar(); ?>
                </div>
            </div>
        </div>
    </div>
<?php get_footer(); ?>
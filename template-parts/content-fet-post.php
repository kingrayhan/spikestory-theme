<div class="featured">
    <article id="post-<?php echo the_ID() ?>" <?php post_class('group post'); ?> >
        <div class="post-inner post-hover">
            <div class="post-thumbnail">
                <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
                    <img src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" style="background:url('<?php echo get_the_post_thumbnail_url(); ?>') no-repeat center center;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;width:720px;height:340px;" class="attachment-thumb-large  wp-post-image nelioefi" alt="<?php the_title(); ?>"> </a>
            </div>
            <div class="post-body">
                <div class="post-meta group">
                    <p class="post-category"><a href="sendible-tips.html" rel="category tag">Product Updates</a></p>
                    <p class="post-date">20 Dec, 2017</p>
                </div>
                <h2 class="post-title"> <a href="finding-success-being-agile.html" rel="bookmark" title="What We Accomplished by Being Agile in 2017"><?php the_title(); ?></a></h2>
                <div class="entry excerpt">
                    <p>2017 was a year of great growth for our team, from bringing in more talent to adopting agile methodologies and expanding the Sendible platform.</p>
                </div>
                <p class="post-tags"><span></span> <a href="sendible.html" rel="tag">sendible</a></p>
            </div>
        </div>
    </article>
</div>


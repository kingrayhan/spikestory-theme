

<article id="post-<?php echo the_ID() ?>" <?php post_class('group post'); ?> style="margin-bottom: 35px;">
    <div class="post-inner post-hover">
        <div class="post-thumbnail">
            <a href="<?php the_permalink(); ?>"
               title="<?php the_title(); ?>"> <img
                        src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7"
                        style="background:url('<?php echo get_the_post_thumbnail_url(); ?>') no-repeat center center;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;width:520px;height:245px;"
                        class="attachment-thumb-medium  wp-post-image nelioefi"
                        alt="<?php the_title(); ?>"> </a>
        </div>
        <div class="post-body">
            <div class="post-meta group">
                <p class="post-category">
                    <?php the_category(' / '); ?>
                <p class="post-date">
                    <?php echo get_the_time('d M, Y'); ?>
                </p>
            </div>
            <h2 class="post-title">
                <a href="<?php the_permalink(); ?>" rel="bookmark" title="<?php the_title(); ?>"><?php the_title(); ?></a>
            </h2>
            <div class="entry excerpt">
                <p>
                    <?php
                    $content = get_the_content();
                    echo wp_trim_words($content, $words = 55);
                    ?></p>
            </div>
            <p class="post-tags">
                <span></span>
                <?php echo get_the_tag_list(); ?>
            </p>
        </div>
    </div>
</article>
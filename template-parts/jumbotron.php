<?php
/**
 * File: jumbotron.php
 * Written By: @KingRayhan <https://github.com/kingRayhan>
 * 1/10/2018 | 8:54 PM
 */

?>

<div class="container group">
    <div class="container-inner">
        <div class="group pad">
            <h2 class="site-description">Social Media News, Articles &amp; Best Practices</h2>
        </div>
    </div>
</div>
